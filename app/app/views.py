from django.views.generic.base import TemplateView


class Home(TemplateView):
    template_name = 'app/base.html'

    def get_context_data(self, **kwargs):
        return super().get_context_data(**kwargs)
